%{

#include<stdio.h>

%}


%token VOID INT FOR DO WHILE IF ELSE MAIN 
%token LETTER ASSIGNMENT DIGIT UNDERSCORE 
%token GT LT GEQ LEQ LPAREN RPAREN LBRACE RBRACE
%token MINUS PLUS STAR EQUAL NOTEQUAL 
%token SEMICOLON COMMA AND OR NOT RETURN


%%
program : INT MAIN LPAREN RPAREN LBRACE statement RBRACE
        | variable_declaration SEMICOLON program
        | function_declaration program
        ;

function_declaration : INT identifier LPAREN RPAREN LBRACE statement RBRACE
                     | INT identifier LPAREN variable_list RPAREN LBRACE statement RBRACE
                     | VOID identifier LPAREN RPAREN LBRACE statement RBRACE
                     | VOID identifier LPAREN variable_list RPAREN LBRACE statement RBRACE
                     ;
                     
variable_list : INT identifier
              | variable_list SEMICOLON INT identifier
	      ;

statement : simple_statement
          | LBRACE statement RBRACE
          | statement statement
          | IF LPAREN expression RPAREN  LBRACE statement RBRACE 
          | IF LPAREN expression RPAREN  LBRACE statement RBRACE ELSE LBRACE statement RBRACE 
          | IF LPAREN expression RPAREN LBRACE statement RBRACE  ELSE  simple_statement
          | IF LPAREN expression RPAREN simple_statement ELSE  LBRACE statement RBRACE
          | FOR LPAREN variable_assignment SEMICOLON expression SEMICOLON identifier ASSIGNMENT expression RPAREN statement
          | WHILE LPAREN expression RPAREN statement 
          | DO statement WHILE LPAREN expression RPAREN SEMICOLON     
          ;
 
simple_statement : SEMICOLON
                 | variable_assignment SEMICOLON
                 | function_call SEMICOLON
                 | RETURN SEMICOLON
                 | RETURN expression SEMICOLON   
                 | IF LPAREN expression RPAREN simple_statement
                 | IF LPAREN expression RPAREN simple_statement  ELSE simple_statement  
                 | FOR LPAREN variable_assignment SEMICOLON expression SEMICOLON identifier ASSIGNMENT expression RPAREN simple_statement
                 ;

variable_assignment : variable_declaration
                    | identifier ASSIGNMENT expression 
	            ;

variable_declaration  : INT identifier 
                      | INT identifier ASSIGNMENT expression
                      | variable_declaration COMMA identifier 
                      | variable_declaration COMMA identifier ASSIGNMENT expression  
	              ;

expression : subexpression
           | MINUS expression
           | NOT expression
	   | subexpression sign expression 
	   | LPAREN expression RPAREN
	   | LPAREN expression RPAREN sign expression
	   ;

subexpression : identifier
           | function_call
           | number	      
           ;

function_call : identifier LPAREN  RPAREN
              | identifier LPAREN param_list RPAREN
	      ; 

param_list : identifier
           | number
           | param_list COMMA identifier
           | param_list COMMA number
           ;


identifier : LETTER
	   | identifier LETTER
	   | identifier DIGIT
           | identifier UNDERSCORE 
           ;

number : DIGIT
       | MINUS DIGIT
       | number DIGIT
       ;


sign : PLUS
     | MINUS
     | STAR
     | GT
     | LT
     | GEQ
     | LEQ
     | EQUAL 
     | NOTEQUAL
     | AND 
     | OR 
     | NOT	
     ; 		 	

%%

extern int line_number;
extern int yylineno;

void yyerror(char const *msg)
{
    fprintf(stderr, "%s: line %d\n", 
            msg, line_number);
}


int main() {
   extern int init(); 
   extern FILE *yyin;
   extern int yylex(); 

   yyin = fopen("example.cpp", "r");

   if (yyparse()) {
    	printf("Error in example.cpp!"); 
   }
   else {
        printf("Сorrect example.cpp!"); 
   }

   fclose(yyin);
   getchar();
   return 0;
}
