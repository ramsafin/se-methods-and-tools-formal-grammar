# se-methods-and-tools-formal-grammar

## Installation

- Install lex and yacc (on Ubuntu):

```shell
apt-get install bizon
apt-get install flex
apt-get install byacc
```

## Build and Run

- Write the code into the ["example.cpp"](example.cpp)

```shell
lex cpp.l
yacc -d cpp.y
gcc lex.yy.c y.tab.c -o check_grammar
./check_grammar
```





